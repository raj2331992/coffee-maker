# Coffee Machine


## Installation

#### Install Open JDK

```bash
sudo apt-get update
sudo apt-get install openjdk-8-jdk
```

#### Run Coffee Machine
You need to compile the code and then you can run the code with command line argument with the test case you want to run
```bash
git clone https://raj2331992@bitbucket.org/raj2331992/coffee-maker.git
cd coffee-maker
javac -cp ".:lib/*" -d ./ $(find ./src/* | grep .java) -Xlint:unchecked
java -cp ".:lib/*" CoffeeMachineTestDriver test1.json
java -cp ".:lib/*" CoffeeMachineTestDriver test2.json
java -cp ".:lib/*" CoffeeMachineTestDriver test3.json
```


## Usage
Coffee Machine has outlets specified count_n, initial available ingredient quantities
specified in total_items_quantity, quantity of ingredients required to prepare each beverage,
in beverages, beverages to be served in serve beverage.

To create more realistic simulation of beverage being prepared and served
we can add beverage_prepare_time and beverage_serve_time as delays which is in milli seconds.
If we don't add these delays all beverages will be created to quickly to perceive any thing.

To refill ingredients in coffee machine we can use increase_ingredients
and provide a list of ingredients and at what time they need to added after the machine starts.


Here is an example test case
```json
{
  "machine": {
    "outlets": {
      "count_n": 2
    },
    "total_items_quantity": {
      "hot_water": 500,
      "hot_milk": 100,
      "ginger_syrup": 100,
      "sugar_syrup": 200,
      "tea_leaves_syrup": 100
    },
    "beverage_prepare_time": 2000,
    "beverage_serve_time": 1000,
    "beverages": {
      "hot_tea": {
        "hot_water": 200,
        "hot_milk": 100,
        "ginger_syrup": 10,
        "sugar_syrup": 10,
        "tea_leaves_syrup": 30
      },
      "hot_coffee": {
        "hot_water": 100,
        "ginger_syrup": 30,
        "hot_milk": 400,
        "sugar_syrup": 50,
        "tea_leaves_syrup": 30
      },
      "black_tea": {
        "hot_water": 300,
        "ginger_syrup": 30,
        "sugar_syrup": 50,
        "tea_leaves_syrup": 30
      },
      "green_tea": {
        "hot_water": 100,
        "ginger_syrup": 30,
        "sugar_syrup": 50,
        "green_mixture": 30
      }
    },
    "serve_beverages": [
      "hot_tea",
      "black_tea",
      "green_tea",
      "hot_coffee"
    ],
    "increase_ingredients": [
      {
        "increase_total_items_quantity": {
          "hot_water": 500,
          "hot_milk": 400,
          "green_mixture": 400
        },
        "increase_total_items_quantity_delay": 1000
      }
    ]
  }
}

```

the output response for the following test case is
```$xslt
Preparing black_tea
Preparing hot_tea
hot_milk quantity is low, current availability:0
hot_water quantity is low, current availability:0
Increased hot_milk quantity by 400, current availability:400
Increased green_mixture quantity by 400, current availability:800
Increased hot_water quantity by 500, current availability:500
black_tea prepared
hot_tea prepared
Preparing green_tea
Preparing hot_coffee
hot_milk quantity is low, current availability:0
tea_leaves_syrup quantity is low, current availability:10
ginger_syrup quantity is low, current availability:0
green_tea prepared
hot_coffee prepared
```

All the tests are available in tests folder and a description each test case is present in testcase-description file
