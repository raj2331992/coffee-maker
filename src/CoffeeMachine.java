import exceptions.BeverageNotFoundException;
import factories.abstractions.BeverageFactory;
import factories.abstractions.IngredientStore;
import models.abstractions.Beverage;
import models.abstractions.Ingredient;

import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;

/***
 * Coffee Machine class provides all the interface methods required to prepare a beverage
 * We initialize the Coffee machine with outlets count, time required to prepare and serve beverages
 * Coffee Machine can create any type of beverage specified by beverage factory
 * thus we can switch between different factories like ChaiPoint, CafeCoffeeDay or even have combinations of them in one machine
 */
public class CoffeeMachine {

    private static IngredientStore ingredientStore;
    private static BeverageFactory beverageFactory;

    private static Integer preparationTime;
    private static Integer serveTime;


    private static Semaphore outlets;

    private static ExecutorService executorService;


    private CoffeeMachine(IngredientStore ingredientStore, BeverageFactory beverageFactory, Integer outlets, Integer preparationTime, Integer serveTime) {
        CoffeeMachine.ingredientStore = ingredientStore;
        CoffeeMachine.beverageFactory = beverageFactory;
        CoffeeMachine.outlets = new Semaphore(outlets);
        CoffeeMachine.preparationTime = preparationTime;
        CoffeeMachine.serveTime = serveTime;
        CoffeeMachine.executorService = Executors.newFixedThreadPool(outlets);
    }


    // Creates a Coffee Machine instance
    public static CoffeeMachine createCoffeeMachine(IngredientStore ingredientStore, BeverageFactory beverageFactory, Integer outlets,Integer preparationTime, Integer serveTime){
        if(outlets<=0){
            System.out.println("Outlet count cannot be negative\nInitializing outlet count to 1");
            outlets = 1;

        }
        if(preparationTime<=0){
            System.out.println("Preparation time cannot be negative\nInitializing Preparation time to 1 second");
            preparationTime = 1000;

        }
        if(serveTime<=0){
            System.out.println("Serve time cannot be negative\nInitializing Serve time to 1 second");
            serveTime = 1000;

        }
        return new CoffeeMachine(ingredientStore, beverageFactory, outlets,preparationTime,serveTime);
    }


    // Initialises Coffee machine ingredients available quantity
    public static void initializeStoreIngredients(Set<Ingredient> ingredientList){
        ingredientStore.initializeStore(ingredientList);
    }

    // Initialized Beverage factory with all the beverages it can prepare
    public static void initializeBeverageFactory(Set<Beverage> beverageContentList){
        beverageFactory.initializeBeverageFactory(beverageContentList);
    }

    // Serve list of beverages
    public static void serveBeverages(List<String> beverages){
        for(String beverageName:beverages){
            executorService.submit(() -> {
                try {
                    // try to acquire a coffee machine outlet
                    // we use a counting semaphore set to number of outlets to achieve this
                    // if we are not able to acquire outlet we try after 500 ms
                    while(!outlets.tryAcquire()) {
                        Thread.sleep(500);
                    }

                    // if we are able to acquire a slot
                    // we get ingredients from store we prepare and serve the beverage and release the outlet
                    // if we are unable to get all ingredients an exception is thrown and we release the outlet
                    System.out.println(String.format("Preparing %s",beverageName));
                    Beverage beverage = beverageFactory.getBeverage(beverageName);
                    beverage.gatherIngredients();
                    beverage.prepareBeverage(preparationTime);
                    beverage.serveBeverage(serveTime);

                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (BeverageNotFoundException e) {
                    System.out.println(e.getMessage());
                } finally {
                    outlets.release();
                }
            });
        }
    }

    // Increase the quantity of ingredients in store after a delay (in milli seconds)
    public static void increaseStoreIngredient(Set<Ingredient>ingredients, Integer delay) throws InterruptedException {
        Thread.sleep(delay);
        synchronized (ingredientStore) {
            ingredients.forEach(ingredient -> {
                ingredientStore.incrementQuantity(ingredient);
            });
        }
    }

    public static void shutdown() {
        executorService.shutdown();
    }
}
