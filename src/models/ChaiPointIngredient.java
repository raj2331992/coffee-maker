package models;


import models.abstractions.Ingredient;

/***
 * Concrete implementation of Ingredient class
 */
public class ChaiPointIngredient extends Ingredient {

    public ChaiPointIngredient(String name, Integer quantity) {
        super(name,quantity);
    }


}
