package models;

import factories.ChaiPointIngredientStore;
import models.abstractions.Beverage;
import models.abstractions.Ingredient;

import java.util.Set;

public class ChaiPointBeverage extends Beverage {
    public ChaiPointBeverage(String name, Set<Ingredient> ingredientList){
        super(name,ingredientList, ChaiPointIngredientStore.getInstance());
    }

    // we create a dummy prepare method which just waits for a specific time
    // this method simulates the real time delay which happens while preparing beverage
    public void prepareBeverage(Integer prepareTime) throws InterruptedException {
        if(prepareTime==null){
            prepareTime =0;
        }
        Thread.sleep(prepareTime);
    }
}
