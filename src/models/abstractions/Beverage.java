package models.abstractions;

import exceptions.IngredientUnavailableException;
import factories.abstractions.IngredientStore;

import java.util.Set;

/***
 * Abstract beverage class which store information relating to beverage and methods to prepare it
 * Concrete classes like Chai Point Beverage or Cafe Coffee Day beverage can have their own ingredients, preparation methods
 */
public abstract class Beverage {
    private String name;
    private Set<Ingredient> ingredientList;

    private IngredientStore ingredientStore;

    public Beverage(String name, Set<Ingredient> ingredientList, IngredientStore ingredientStore) {
        this.name = name;
        this.ingredientList = ingredientList;
        this.ingredientStore = ingredientStore;
    }

    public String getName() {
        return name;
    }

    public Set<Ingredient> getIngredientList() {
        return ingredientList;
    }


    abstract public void prepareBeverage(Integer prepareTime) throws InterruptedException;

    // we create a dummy serve method which just waits for a specific time
    // this method simulates the real time delay which happens while serving beverage
    public void serveBeverage(Integer serveTime) throws InterruptedException {
        if(serveTime==null){
            serveTime =0;
        }
        Thread.sleep(serveTime);
        System.out.println(String.format("%s prepared",name));
    }

    // method to gather all the ingredients required to prepare beverage
    // we first lock the store and check if all the ingredients are available and then decrement them
    // this avoids data inconsistency and deadlocks from occurring
    public void gatherIngredients(){
        // locks the ingredient store
        synchronized (ingredientStore){

            // check if all ingredients are available else throw exception
            for(Ingredient ingredient : ingredientList){
                if(!ingredientStore.checkIngredientAvailability(ingredient)){
                    System.out.println(String.format("%s cannot be prepared because %s is not available",name,ingredient.getName()));
                    throw new IngredientUnavailableException(String.format("%s cannot be prepared because %s is not available",name,ingredient.getName()));
                }
            }
            // decrement quantity of all the ingredients from store
            for(Ingredient ingredient : ingredientList){
                ingredientStore.decrementQuantity(ingredient);
            }
        }
    }

}
