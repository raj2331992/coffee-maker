import factories.ChaiPointBeverageFactory;
import factories.ChaiPointIngredientStore;
import factories.abstractions.BeverageFactory;
import factories.abstractions.IngredientStore;
import models.abstractions.Beverage;
import models.abstractions.Ingredient;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import utils.DataExtractionUtils;

import java.util.List;
import java.util.Set;

/***
 * This Class extracts data from test cases and runs the coffee machine to prepare beverages
 */
@SuppressWarnings("unchecked")
public class CoffeeMachineTestDriver {

    public static void main(String[] args) {

        try {
            // if no agruments are provided run default test
            String testFileName = "test1.json";

            // Takes test file name from command line arguments
            if(args.length>0){
                testFileName = args[0];
            }

            //Parse json test file and get all the Coffee Machine Data
            JSONObject testData = DataExtractionUtils.extractJsonFromFile(testFileName,"tests/");
            JSONObject machine = (JSONObject) testData.get("machine");

            // Extract outlet count
            Integer outletCount = DataExtractionUtils.extractOutletCount(machine);

            // Extract initial ingredients quantity for coffee machine
            Set<Ingredient> initialStoreIngredients = DataExtractionUtils.extractInitialStoreIngredients(machine);

            // Extract ingredients required to make a beverage
            Set<Beverage> beveragesContents = DataExtractionUtils.extractBeverageContentsList(machine);

            // Extract list of beverages to be served
            List<String> beverages = DataExtractionUtils.extractBeveragesToServe(machine);

            // Extract time duration required to prepare and serve beverage
            // If we dont put delay all outputs will be displayed at once
            Integer beveragePreparationTime = DataExtractionUtils.extractBeveragePreparationTime(machine);
            Integer beverageServeTime = DataExtractionUtils.extractBeverageServeTime(machine);

            // Initialize Ingredient  and Beverage factories which will be required by Coffee Machine to prepare beverages
            // In future we may may want to create suppose a Cafe Coffee Day Machine so we can easily extend our design for it
            IngredientStore chaiPointIngredientStore = ChaiPointIngredientStore.getInstance();
            BeverageFactory chaiPointBeverageFactory = ChaiPointBeverageFactory.getInstance();

            // Create a Coffee Machine and serve beverages
            CoffeeMachine.createCoffeeMachine(chaiPointIngredientStore,chaiPointBeverageFactory,outletCount,beveragePreparationTime,beverageServeTime);
            CoffeeMachine.initializeStoreIngredients(initialStoreIngredients);
            CoffeeMachine.initializeBeverageFactory(beveragesContents);
            CoffeeMachine.serveBeverages(beverages);


            // Increase Ingredient quantity in Coffee machine
            // We read list of ingredients whose quantity need to be increased and at time specified by delay (specified in milli seconds)
            // We might want to increase hot_water by 20 units after 3 secs of starting the coffee machine
            // Delay is only introduced to test whether we are able to increase ingredients at run time from test data
            JSONArray incrementStoreIngredientList = DataExtractionUtils.extractIncrementStoreIngredientList(machine);
            incrementStoreIngredientList.parallelStream().forEach( increaseIngredient -> {
                    Integer delay = DataExtractionUtils.extractIncrementStoreIngredientsDelay((JSONObject)increaseIngredient);
                    if(delay!=null){
                        Set<Ingredient> ingredients = DataExtractionUtils.extractIncrementStoreIngredient((JSONObject) increaseIngredient);
                        try {
                            CoffeeMachine.increaseStoreIngredient(ingredients,delay);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
            });

            //After all beverages are served we shutdown the machine
            CoffeeMachine.shutdown();

        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}
