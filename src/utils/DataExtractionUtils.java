package utils;


import exceptions.IngredientListEmptyException;
import factories.ChaiPointBeverageFactory;
import factories.ChaiPointIngredientFactory;
import factories.abstractions.BeverageFactory;
import factories.abstractions.IngredientFactory;
import models.abstractions.Beverage;
import models.abstractions.Ingredient;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/***
 * Utility Class to extract JSON data from text file and convert it into Java Objects
 */
@SuppressWarnings("unchecked")
public class DataExtractionUtils {

    // Extract JSON data from given file
    public static JSONObject extractJsonFromFile(String file, String path){
        JSONParser jsonParser = new JSONParser();
        try {
            Object object =jsonParser.parse(new FileReader(path+file));

            return (JSONObject) object;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    // Check if key exists in JSON
    private static void checkDataExists(JSONObject jsonObject, String key) {
        if(!jsonObject.containsKey(key)){
            throw new RuntimeException(String.format("%s is missing in test json",key));
        }
    }


    // Extract outlets count
    public static Integer extractOutletCount(JSONObject machine){
        checkDataExists(machine,"outlets");
        JSONObject outlets = (JSONObject) machine.get("outlets");

        checkDataExists(outlets,"count_n");
        Long outletCount = (Long) outlets.get("count_n");
        return outletCount.intValue();
    }

    //Extracts Beverage Preparation time which should milliseconds
    public static Integer extractBeveragePreparationTime(JSONObject machine){
        Long beveragePreparationTime = (Long) machine.get("beverage_prepare_time");
        if(beveragePreparationTime!=null){
            return beveragePreparationTime.intValue();
        }
        return 0;
    }

    //Extracts Beverage Serving time which should milliseconds
    public static Integer extractBeverageServeTime(JSONObject machine){
        Long beverageServeTime = (Long) machine.get("beverage_serve_time");
        if(beverageServeTime!=null){
            return beverageServeTime.intValue();
        }
        return 0;
    }

    // Extract Total Item Quantity
    public static Set<Ingredient> extractInitialStoreIngredients(JSONObject machine){
        checkDataExists(machine,"total_items_quantity");
        JSONObject totalItemsQuantity = (JSONObject) machine.get("total_items_quantity");

        Set<Ingredient> ingredients = new HashSet<>();

        totalItemsQuantity.forEach((ingredientName,ingredientQuantity) ->{
            IngredientFactory ingredientFactory = ChaiPointIngredientFactory.getInstance();
            Ingredient ingredient = ingredientFactory.createIngredient((String) ingredientName, ((Long) ingredientQuantity).intValue());
            ingredients.add(ingredient);
        });
        return ingredients;
    }

    // Reads all the ingredients for a beverage and create a beverage object from beverage factory
    // Returns all the beverage which the coffee machine is capable of producing
    public static Set<Beverage> extractBeverageContentsList(JSONObject machine){
        checkDataExists(machine,"beverages");
        JSONObject beverageContentsJson = (JSONObject) machine.get("beverages");

        Set<Beverage> beverages = new HashSet<>();

        beverageContentsJson.forEach((beverageName,ingredientsList) ->{
            IngredientFactory ingredientFactory = ChaiPointIngredientFactory.getInstance();
            Set<Ingredient> beverageIngredientList = new HashSet<>();

            ((JSONObject)ingredientsList).forEach((ingredientName,ingredientQuantity) -> {
                Ingredient ingredient = ingredientFactory.createIngredient((String) ingredientName, ((Long) ingredientQuantity).intValue());
                beverageIngredientList.add(ingredient);
            });

            BeverageFactory beverageFactory = ChaiPointBeverageFactory.getInstance();
            try {
                Beverage beverage = beverageFactory.createBeverage((String) beverageName, beverageIngredientList);
                beverages.add(beverage);
            } catch (IngredientListEmptyException e) {
                e.printStackTrace();
            }

        });
        return beverages;
    }

    // Extract all the beverages the machine needs to prepare
    public static List<String> extractBeveragesToServe(JSONObject machine){
        checkDataExists(machine,"serve_beverages");
        JSONArray beveragesToServe = (JSONArray) machine.get("serve_beverages");
        List<String> beverages = new ArrayList<>();
        beveragesToServe.forEach(beverageName -> beverages.add((String)beverageName));
        return beverages;

    }

    // Extract Increase Ingredient JSON Array
    public static JSONArray extractIncrementStoreIngredientList(JSONObject machine){
        JSONArray increaseIngredientsList = new JSONArray();
        if(machine.containsKey("increase_ingredients")) {
            increaseIngredientsList =  (JSONArray) machine.get("increase_ingredients");
        }
        return increaseIngredientsList;
    }

    // Extract Ingredients whose quantity needs to be increased
    public static Set<Ingredient> extractIncrementStoreIngredient(JSONObject data){
        JSONObject increasetemsQuantity = (JSONObject) data.get("increase_total_items_quantity");
        Set<Ingredient> ingredients = new HashSet<>();
        if(increasetemsQuantity!=null) {
            increasetemsQuantity.forEach((ingredientName, ingredientQuantity) -> {
                IngredientFactory ingredientFactory = ChaiPointIngredientFactory.getInstance();
                Ingredient ingredient = ingredientFactory.createIngredient((String) ingredientName, ((Long) ingredientQuantity).intValue());
                ingredients.add(ingredient);
            });
        }
        return ingredients;
    }

    // Extract time at which the ingredients need to be increased
    public static Integer extractIncrementStoreIngredientsDelay(JSONObject machine){
        Long delay = (Long) machine.get("increase_total_items_quantity_delay");
        if(delay!=null){
            return delay.intValue();
        }
        return null;
    }
}
