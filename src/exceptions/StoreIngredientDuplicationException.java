package exceptions;

public class StoreIngredientDuplicationException extends RuntimeException {
    public StoreIngredientDuplicationException(String message) {
        super(message);
    }
}
