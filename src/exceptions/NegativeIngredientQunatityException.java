package exceptions;

public class NegativeIngredientQunatityException extends RuntimeException {
    public NegativeIngredientQunatityException(String message) {
        super(message);
    }
}
