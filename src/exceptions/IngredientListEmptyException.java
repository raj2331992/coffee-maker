package exceptions;

public class IngredientListEmptyException extends RuntimeException {
    public IngredientListEmptyException(String message) {
        super(message);
    }
}
