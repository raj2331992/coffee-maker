package exceptions;

public class IngredientUnavailableException extends RuntimeException {
    public IngredientUnavailableException(String message) {
        super(message);
    }
}
