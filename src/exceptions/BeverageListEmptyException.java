package exceptions;

public class BeverageListEmptyException extends RuntimeException {
    public BeverageListEmptyException(String message) {
        super(message);
    }
}
