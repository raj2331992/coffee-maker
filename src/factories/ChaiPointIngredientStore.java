package factories;


import factories.abstractions.IngredientStore;

/***
 * Creates a concrete store which stores all the available ingredients of coffee machine
 *
 */
public class ChaiPointIngredientStore extends IngredientStore {
    private static volatile ChaiPointIngredientStore instance;
    private static Object mutex = new Object();

    private ChaiPointIngredientStore() {

    }

    // get single instance of this class
    public static ChaiPointIngredientStore getInstance() {
        ChaiPointIngredientStore result = instance;
        if (result == null) {
            synchronized (mutex) {
                result = instance;
                if (result == null)
                    instance = result = new ChaiPointIngredientStore();
            }
        }
        return result;
    }

}
