package factories.abstractions;


import exceptions.IngredientListEmptyException;
import exceptions.IngredientNotFoundException;
import exceptions.IngredientUnavailableException;
import exceptions.NegativeIngredientQunatityException;
import exceptions.StoreIngredientDuplicationException;
import models.abstractions.Ingredient;
import org.apache.commons.collections4.CollectionUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/***
 * Abstract store which provides methods to initialize, increment, decrement ingredient quantities
 * We can create concrete implementations like ChaiPoint Store or Cafe Coffee Day store which store their own ingredients
 */
public abstract class IngredientStore {

    private Map<String, IngredientStore.StoreIngredient> ingredientStoreMap = new HashMap<>();
    private static final int thresholdLevel = 20;

    // to store ingredient information present in the store
    private class StoreIngredient{

        private String name;
        private Integer availableQuantity;

        private StoreIngredient(String name, Integer availableQuantity) {
            this.name = name;
            this.availableQuantity = availableQuantity;
        }
    }

    // initialize the store with initial quantity of ingredients in coffee machine
    public void initializeStore(Set<Ingredient> ingredientList){
        if(CollectionUtils.isEmpty(ingredientList)){
            throw new IngredientListEmptyException(String.format("Cannot initialize store with no ingredients"));
        }
        ingredientList.forEach(ingredient -> {
            if(ingredientStoreMap.containsKey(ingredient.getName())){
                throw new StoreIngredientDuplicationException(String.format("Cannot initialize store found duplicate ingredient %s",ingredient.getName()));
            }
            IngredientStore.StoreIngredient storeIngredient = new IngredientStore.StoreIngredient(ingredient.getName(),ingredient.getQuantity());
            ingredientStoreMap.put(ingredient.getName(),storeIngredient);
        });
    }

    // Check if ingredient is available in store
    public Boolean checkIngredientAvailability(Ingredient ingredient){
        if(ingredient==null){
            throw new IllegalArgumentException("Ingredient cannot be null");
        }
        Integer quantity = ingredient.getQuantity();
        String ingredientName = ingredient.getName();
        if (ingredientStoreMap.containsKey(ingredientName)) {
            Integer availableQuantity = ingredientStoreMap.get(ingredientName).availableQuantity;
            if(quantity>availableQuantity){
                return Boolean.FALSE;
            }else{
                return Boolean.TRUE;
            }
        }else{
            return Boolean.FALSE;
        }
    }

    // Increment the ingredient quantity in store
    public void incrementQuantity(Ingredient ingredient){
        String ingredientName = ingredient.getName();
        Integer quantity = ingredient.getQuantity();
        if(quantity <0){
            throw new NegativeIngredientQunatityException(String.format("%s increment quantity %d cannot be negative",ingredient,quantity));
        }

        if(!ingredientStoreMap.containsKey(ingredientName)){
            IngredientStore.StoreIngredient storeIngredient = new IngredientStore.StoreIngredient(ingredient.getName(),ingredient.getQuantity());
            ingredientStoreMap.put(ingredient.getName(),storeIngredient);
        }

        IngredientStore.StoreIngredient storeIngredient = ingredientStoreMap.get(ingredientName);
        storeIngredient.availableQuantity += quantity;
        System.out.println(String.format("Increased %s quantity by %d, current availability:%d",ingredientName,quantity,storeIngredient.availableQuantity));
    }

    // Decrement the ingredient quantity in store
    public void decrementQuantity(Ingredient ingredient){
        Integer quantity = ingredient.getQuantity();
        String ingredientName = ingredient.getName();
        if (ingredientStoreMap.containsKey(ingredientName)) {
            Integer availableQuantity = ingredientStoreMap.get(ingredientName).availableQuantity;
            if(quantity>availableQuantity){
                throw new IngredientUnavailableException(String.format("%s decrement quantity %d greater than available quantity %d",ingredientName,quantity,availableQuantity));
            }
        }else{
            throw new IngredientNotFoundException(String.format("Cannot find ingredient %s in store", ingredientName));
        }

        IngredientStore.StoreIngredient storeIngredient = ingredientStoreMap.get(ingredientName);
        storeIngredient.availableQuantity -= quantity;
        if(storeIngredient.availableQuantity<= thresholdLevel){
            System.out.println(String.format("%s quantity is low, current availability:%d",ingredientName,storeIngredient.availableQuantity));
        }
    }
}
