package factories.abstractions;


import models.abstractions.Ingredient;

/***
 * Abstract factory which provides method to create ingredient
 * Each concrete implementations specifies how to create their ingredient
 * ChaiPoint Ingredient Factory or Cafe Coffee Day Ingredient Factory can create ingredients according to their beverage requirements
 */
public abstract class IngredientFactory {

    public abstract Ingredient createIngredient(String name, Integer quantity);

}
