package factories.abstractions;

import exceptions.BeverageListEmptyException;
import exceptions.BeverageNotFoundException;
import exceptions.IngredientListEmptyException;
import models.abstractions.Beverage;
import models.abstractions.Ingredient;
import org.apache.commons.collections4.CollectionUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/***
 * Abstract beverage factory which provides method to define set of beverages which it can prepare, get beverages
 * Each concrete implementations specifies how to create their beverage
 * ChaiPoint Ingredient Factory or Cafe Coffee Day Ingredient Factory can create their beverage own beverages with different ingredients
 */
public abstract class BeverageFactory {

    Map<String,Beverage> beverages = new HashMap<>();

    // Initialize factory with all the beverages the coffee machine can prepare
    public void initializeBeverageFactory(Set<Beverage> beverageContentList){
        if(CollectionUtils.isNotEmpty(beverageContentList)) {
            beverageContentList.forEach(beverage -> beverages.put(beverage.getName(), beverage));
        }else{
            throw new BeverageListEmptyException(String.format("Beverage list to initialize store is empty"));
        }
    }

    // Get the beverage with ingredients and quantity required to prepare the beverage
    public Beverage getBeverage(String name) throws BeverageNotFoundException{
        if(name==null){
            throw new IllegalArgumentException("Beverage name is null");
        }
        if(beverages.containsKey(name)){
            return beverages.get(name);
        }else{
            throw new BeverageNotFoundException(String.format("%s is not served by Machine",name));
        }
    }

    public abstract Beverage createBeverage(String name, Set<Ingredient> ingredientList) throws IngredientListEmptyException;
}
