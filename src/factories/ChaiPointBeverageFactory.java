package factories;

import exceptions.IngredientListEmptyException;
import factories.abstractions.BeverageFactory;
import models.ChaiPointBeverage;
import models.abstractions.Beverage;
import models.abstractions.Ingredient;
import org.apache.commons.collections4.CollectionUtils;

import java.util.Set;

/***
 * Concrete factory which creates a Chai Point Beverage
 */
public class ChaiPointBeverageFactory extends BeverageFactory {

    private static volatile ChaiPointBeverageFactory instance;
    private static Object mutex = new Object();

    private ChaiPointBeverageFactory() {
    }

    // get single instance of this class
    public static ChaiPointBeverageFactory getInstance() {
        ChaiPointBeverageFactory result = instance;
        if (result == null) {
            synchronized (mutex) {
                result = instance;
                if (result == null)
                    instance = result = new ChaiPointBeverageFactory();
            }
        }
        return result;
    }

    @Override
    public Beverage createBeverage(String name, Set<Ingredient> ingredientList){
        if(name==null){
            throw new IllegalArgumentException("name cannot be null");
        }
        if(CollectionUtils.isEmpty(ingredientList)){
            throw new IngredientListEmptyException(String.format("Ingredient List Empty for Beverage %s",name));
        }
        return new ChaiPointBeverage(name, ingredientList);
    }


}
