package factories;

import exceptions.NegativeIngredientQunatityException;
import factories.abstractions.IngredientFactory;
import models.ChaiPointIngredient;
import models.abstractions.Ingredient;

/***
 * Concrete factory which creates ingredients required to create a Chai Point Beverage
 */
public class ChaiPointIngredientFactory extends IngredientFactory {

    private static volatile ChaiPointIngredientFactory instance;
    private static Object mutex = new Object();


    private ChaiPointIngredientFactory(){

    }

    // get single instance of this class
    public static ChaiPointIngredientFactory getInstance() {
        ChaiPointIngredientFactory result = instance;
        if (result == null) {
            synchronized (mutex) {
                result = instance;
                if (result == null)
                    instance = result = new ChaiPointIngredientFactory();
            }
        }
        return result;
    }

    @Override
    public Ingredient createIngredient(String name, Integer quantity) {
        if(quantity<=0){
            throw new NegativeIngredientQunatityException(String.format("Ingredient %s cannot have negative value",name));
        }
        return new ChaiPointIngredient(name, quantity);
    }
}
